# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import random
import string

compass = {
    'N': [0, -1],
    'E': [+1, 0],
    'S': [0, +1],
    'W': [-1, 0],
    'NE': [-1, +1],
    'SE': [+1, +1],
    'SW': [+1, -1],
    'NW': [-1, -1]
}

computers_ships = {
    'C': 5,  # Carrier
    'D': 4,  # Destroyer
    'M': 3,  # Minesweeper
    'F': 3,  # Frigate
    'S': 2   # Submarine
}
opponents_ships = computers_ships.copy()


# This is a 2D array (row index first), indicating the status of each cell:
# . empty untouched cell
# * splash - cell has had a shell land in it, and the cell is empty
# ? splash close to a ship - cell is empty, has been hit by a shell but neighbours a cell that has a ship in it
#
# The same principle applies for all ships:
# c cell is part of carrier ship that has been hit.
# C cell is part of carrier ship that has not been hit.
computers_board = []
opponents_board = []


def play_game():
    """
    Main loop to play the game.
    :return:
    """
    setup_board(computers_board)
    place_ships_on_board()

    setup_board(opponents_board)

    while True:
        print_board("Computer's board", computers_board, computers_ships)
        move = input("Enter your move: ")
        accept_opponents_move(move)

        if ships_left(computers_ships) == 0:
            print("Opponent wins")
            return

        col, row = determine_computers_move()
        result = input(f'Enter the result for {string.ascii_uppercase[row]}{col} as *, ?, C, D, M, F, S :F2')
        write_to_cell_on_board(opponents_board, result, col, row)

        if result in opponents_ships:
            opponents_ships[result] = opponents_ships[result] - 1
        print_board("Opponent's board", opponents_board, opponents_ships)

        if ships_left(opponents_ships) == 0:
            print("Computer wins")
            return


def setup_board(board):
    """
    Setup the given board to be an array of strings.

    :param board:
    :return:
    """
    for r in range(10):
        board.append('.' * 10)


def print_board(name, board, ships):
    """
    Prints the board to the screen including col refs as capital letters and row refs as single digit numbers between
    0 and 9
    :return: None
    """
    print(name)
    print(' ' + string.ascii_uppercase[:10])
    for row_index, row in enumerate(board, start=0):
        print(str(row_index) + row)
    print(str(ships_left(ships)) + " ship cells remain")
    print(ships)
    print()


def place_ships_on_board():
    """
    Randomly places the ships on the board.
    :return:
    """
    for ship_type, length in computers_ships.items():
        while True:
            heading = random.choice(list(compass.keys())[:4])
            c_start = random.randint(0, 9)
            r_start = random.randint(0, 9)

            if test_placement(c_start, r_start, length, heading):
                place_ship(c_start, r_start, ship_type, heading)
                break


def test_placement(c_start, r_start, length, heading):
    """
    Test whether we can place the ship at the position indicated by c_start, r_start without it being on a cell that
    is already occupied,

    :param c_start: 0..10 - column start position for ship
    :param r_start: 0..10 - row start position for ship
    :param length: 2..5 - length of ship
    :param heading: 'N', 'S', 'E', 'W' - direction in which to layout ship
    :return: True if it is possible to place the ship on the board, otherwise False
    """
    c, r = c_start, r_start
    c_step, r_step = compass[heading]

    for i in range(length):
        if r < 0 or r > 9 or c < 0 or c > 9 or computers_board[r][c] != '.' or check_neighbours(c, r):
            return False
        c = c + c_step
        r = r + r_step
    return True


def place_ship(c_start, r_start, ship_type, heading):
    """
    Add ship at predefined position to board.  Assumes that the position is legitimate

    :param c_start: 0..9 - column start position for ship
    :param r_start: 0..9 - row start position for ship
    :param ship_type: single letter string - type of ship.
    :param heading: 'N', 'S', 'E', 'W' - direction in which to layout ship
    :return: void
    """
    c, r = c_start, r_start
    c_step, r_step = compass[heading]

    for i in range(computers_ships[ship_type]):
        write_to_cell_on_board(computers_board, ship_type, c, r)
        computers_board[r] = "".join((computers_board[r][:c], ship_type, computers_board[r][c + 1:]))
        c = c + c_step
        r = r + r_step


def write_to_cell_on_board(board, value, column, row):
    """
    This updates the spcified cell at (column, row) with the value
    :param board: [][]
    :param value: single letter string
    :param column: 0..9
    :param row: 0..9
    :return:
    """
    board[row] = "".join((board[row][:column], value, board[row][column + 1:]))


def check_neighbours(c_start, r_start):
    """
    Check if any neighbouring cells contain parts of a ship

    :param c_start: 0..9 - column position to be evaluated
    :param r_start: 0..9 - row position to be evaluated
    :return:
    """
    for c_step, r_step in compass.values():
        c = c_start + c_step
        r = r_start + r_step
        if 0 <= c <= 9 and 0 <= r <= 9 and computers_board[r][c] != '.':
            return True
    return False


def accept_opponents_move(move):
    c = string.ascii_uppercase.index(move[0])
    r = int(move[1])

    if computers_board[r][c] == '.':
        if check_neighbours(c, r):
            print("Near miss!!")
            write_to_cell_on_board(computers_board, '?', c, r)
        else:
            print("Splash!")
            write_to_cell_on_board(computers_board, '*', c, r)
    elif computers_board[r][c].isupper():
        ship = computers_board[r][c]
        write_to_cell_on_board(computers_board, ship.lower(), c, r)
        computers_ships[ship] -= 1
        if computers_ships[ship] == 0:
            print(ship + " has been sunk")
        else:
            print(ship + " has been hit")


# The computer is in one of three modes:
# Random - it is trying to find a ship by taking random pot shots on the board
# Hunt - it splashed close to a ship.  (first_hit_col, first_hit_row) defines where it got a close hit.  It is now
#   now searching around a near miss.
computers_play_mode = "Hunt"
first_hit_col = None
first_hit_row = None


def determine_computers_move():
    while True:
        col = random.randint(0, 9)
        row = random.randint(0, 9)

        if opponents_board[row][col] == '.':
            return col, row


def ships_left(ships):
    """
    :return: integer - number of ship cells that have not been hit
    """
    return sum(ships.values())


play_game()
